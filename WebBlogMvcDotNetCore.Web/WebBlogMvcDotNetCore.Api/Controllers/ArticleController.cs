﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebBlogMvcDotNetCore.Service;
using WebBlogMvcDotNetCore.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebBlogMvcDotNetCore.Api.Controllers
{
    
    public class ArticleController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IArticleServices _articleServices;
        public ArticleController(IArticleServices articleServices)
        {
            this._articleServices = articleServices;
        }

        [HttpGet]
        [Route("api/Article/GetArticleById/{id}")]
        public ActionResult GetArticleById(int id)
        {
            Article article =  _articleServices.GetArticle(id);
            if(article != null)
            {
                return Ok(article);
            }
            return NotFound();
            
        }

        [HttpPost, Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("api/Article/CreateArticle")]
        public ActionResult CreateArticle([FromBody] Article model)
        {
            if (ModelState.IsValid)
            {
                var img = getImg(model.Content).Replace("http://localhost:53498/images/", "");
                Article article = new Article
                {
                    IdCategory = 1,
                    Title = model.Title,
                    Content = model.Content,
                    IdUserProfile = model.IdUserProfile,
                    Picture = img,
                    DateCreate = model.DateCreate
                };
                _articleServices.InsertArticle(article);
                return Ok();

            }
            return BadRequest(ModelState);
        }

        public string getImg(string input)
        {
            try
            {
                string imgResult = "";
                imgResult = Regex.Match(input, "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase).Groups[1].Value;
                return imgResult;
            }
            catch
            {
                return "";
            }
        }


    }
}
