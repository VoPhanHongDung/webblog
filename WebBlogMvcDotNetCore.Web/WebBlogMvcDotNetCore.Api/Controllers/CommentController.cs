﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebBlogMvcDotNetCore.Data;
using WebBlogMvcDotNetCore.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebBlogMvcDotNetCore.Api.Controllers
{
  
    public class CommentController : Controller
    {
        private readonly ICommentServices _commentServices;
        public CommentController(ICommentServices commentServices)
        {
            this._commentServices = commentServices;
        }

        // GET: api/values
        [HttpGet]
        [Route("api/Comment/GetCommentById/{idArticle}")]
        public IEnumerable<Comment> GetCommentById(int idArticle)
        {
            return _commentServices.GetComment(idArticle).Take(3);
        }


        [HttpPost, Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("api/Comment/CreateComment")]
        public IActionResult CreateComment([FromBody] Comment model)
        {
            if (ModelState.IsValid)
            {
                Comment comment = new Comment
                {
                    IdArticle = model.IdArticle,
                    Fullname = model.Fullname,
                    DateCreate = model.DateCreate, 
                    DetailComment = model.DetailComment
                };
                 _commentServices.InsertComment(comment);
                return Ok();
                
            }
            return BadRequest(ModelState);

        }





    }
}
