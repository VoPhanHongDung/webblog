﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebBlogMvcDotNetCore.Service;
using WebBlogMvcDotNetCore.Data;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebBlogMvcDotNetCore.Api.Controllers
{
 
    public class HomeController : Controller
    {

        private readonly IArticleServices _articleServices;
        private readonly ICommentServices _commentServices;
        public HomeController(IArticleServices articleServices , ICommentServices commentServices)
        {
            this._articleServices = articleServices;
            this._commentServices = commentServices;
        }


        [HttpGet]
        [Route("api/Home/GetAllArticle")]
        public IEnumerable<Article> GetAllArticle()
        {
            IEnumerable<Article> articleList = _articleServices.GetArticle();
            return articleList;
        }


        [HttpGet]
        [Route("api/Home/GetRecentComment")]
        public IEnumerable<Comment> GetRecentComment()
        {
            IEnumerable<Comment> recentComment = _commentServices.GetRecentComment();
            return recentComment;
        }

        [HttpGet]
        [Route("api/Home/GetRecentArticle")]
        public IEnumerable<Article> GetRecentArticle()
        {
            IEnumerable<Article> recentArticle = _articleServices.GetRecentArticle();
            return recentArticle;
        }







    }
}
