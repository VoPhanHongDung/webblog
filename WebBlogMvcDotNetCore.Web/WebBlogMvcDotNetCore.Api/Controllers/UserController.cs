﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebBlogMvcDotNetCore.Service;
using WebBlogMvcDotNetCore.Data;
using WebBlogMvcDotNetCore.Api.Models;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebBlogMvcDotNetCore.Api.Controllers
{


    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserProfileService _userProfileService;

        public UserController(IUserService userService, IUserProfileService userProfileService)
        {
            this._userService = userService;
            this._userProfileService = userProfileService;
        }


        [HttpPost]
        [Route("api/User/RegisterUser")]
        public IActionResult RegisterUser([FromBody] User model)
        {
            if (ModelState.IsValid)
            {
                User userEntity = new User
                {
                    UserName = model.UserName,
                    Password = model.Password
                };
                _userService.InsertUser(userEntity);

                if (userEntity.Id != 0)
                {
                    return  Ok( userEntity.Id);
                }
                else
                {
                    return NotFound();
                }
            }

            return BadRequest(ModelState);
        }


        [HttpPost]
        [Route("api/User/RegisterUserProfile")]
        public IActionResult RegisterUserProfile([FromBody] UserProfile model)
        {
            if (ModelState.IsValid)
            {
                UserProfile user = _userProfileService.GetUserProfileByIdUser(model.IdUser);
                if (user == null)
                {
                    UserProfile userProfileNew = new UserProfile
                    {
                        LastName = model.LastName,
                        FirstName = model.FirstName,
                        IdUser = model.IdUser,
                        Email = model.Email,
                        Address = model.Address
                    };

                    _userProfileService.InsertUserProfile(userProfileNew);
                    return Ok(userProfileNew.Id);
                }
            }
            return BadRequest(ModelState);

        }

        [HttpPost]
        [Route("api/User/Login")]
        public IActionResult Login([FromBody]User user)
        {
            if (user == null)
            {
                return BadRequest("Invalid client request");
            }

            User userTemp = _userService.SignIn(user.UserName, user.Password);

            if ( userTemp != null)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:53498",
                    audience: "http://localhost:53498",
                    claims: new List<Claim>(),
                    expires: DateTime.Now.AddMinutes(5),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                UserProfile userProfile = _userProfileService.GetUserProfileByIdUser(userTemp.Id);
                return Ok(new { Token = tokenString , userTemp.UserName , userProfile.Id});
            }
            else
            {
                return Unauthorized();
            }
        }

       

   



       
    }



}
