﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebBlogMvcDotNetCore.Api.Models
{
    public class ArticleModel
    {
        [HiddenInput]
        public Int64 Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Content { get; set; }


        public string Picture { get; set; }

        [HiddenInput]
        [Required]
        public Int64 IdCategory { get; set; }

        [HiddenInput]
        [Required]
        public Int64 IdUserProfile { get; set; }

        [DataType(DataType.Date)]
        [HiddenInput]
        public DateTime DateCreate { get; set; }
    }
}
