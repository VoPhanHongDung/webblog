﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBlogMvcDotNetCore.Data;

namespace WebBlogMvcDotNetCore.Api.Models
{
    public class CurrencyArticleAndComment
    {
        public Article article { get; set; }
        public IEnumerable<Comment> comments { get; set; }
    }
}
