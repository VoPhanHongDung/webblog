﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebBlogMvcDotNetCore.Api.Models
{
    public class UserProfileModel
    {
        [HiddenInput]
        public Int64 Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Address { get; set; }

        [HiddenInput]
        public Int64 IdUser { get; set; }
    }
}
