﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebBlogMvcDotNetCore.Data
{
    public class Article
    {
        public Int64 Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Picture { get; set; }
        public Int64 IdCategory { get; set; }
        public Int64 IdUserProfile { get; set; }
        public DateTime DateCreate { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public virtual Category Category { get; set; }

        public ICollection<Comment> Comment { get; set; }
    }
}
