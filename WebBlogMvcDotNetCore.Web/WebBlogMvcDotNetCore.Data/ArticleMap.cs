﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebBlogMvcDotNetCore.Data
{
    public class ArticleMap
    {
        public ArticleMap(EntityTypeBuilder<Article> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.Title).IsRequired();
            entityTypeBuilder.Property(t => t.Content).IsRequired();
            entityTypeBuilder.Property(t => t.Picture);
            entityTypeBuilder.Property(t => t.DateCreate);
            

        }
    }
}
