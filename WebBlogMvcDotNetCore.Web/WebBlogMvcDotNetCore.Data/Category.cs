﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBlogMvcDotNetCore.Data
{
    public class Category
    {
        public Int64 Id { get; set; }
        public string DetailCategory { get; set; }
        public string TitleCategory { get; set; }

        public ICollection<Article> Article { get; set; }
    }
}
