﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebBlogMvcDotNetCore.Data
{
    public class CategoryMap
    {
        public CategoryMap(EntityTypeBuilder<Category> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.TitleCategory).IsRequired();
            entityTypeBuilder.Property(t => t.DetailCategory);
            entityTypeBuilder.HasMany(t => t.Article).WithOne(c => c.Category).HasForeignKey(x => x.IdCategory);

        }
    }
}
