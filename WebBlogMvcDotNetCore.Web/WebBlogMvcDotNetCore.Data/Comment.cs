﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WebBlogMvcDotNetCore.Data
{
    public class Comment
    {
        public Int64 Id { get; set; }
        public string DetailComment { get; set; }
        public Int64 IdArticle { get; set; }
        public string Fullname { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreate { get; set; }

        public Article Article { get; set; }
    }
}
