﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebBlogMvcDotNetCore.Data
{
    public class CommentMap
    {
        public CommentMap(EntityTypeBuilder<Comment> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.DetailComment);
            entityTypeBuilder.Property(t => t.Fullname);
            entityTypeBuilder.HasOne(t => t.Article).WithMany(t => t.Comment).HasForeignKey(x => x.IdArticle);
            entityTypeBuilder.Property(t => t.DateCreate);
        }
    }
}
