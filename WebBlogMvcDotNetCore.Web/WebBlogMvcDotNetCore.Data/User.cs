﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebBlogMvcDotNetCore.Data
{
    public class User
    {
        public Int64 Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Roles { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
