﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebBlogMvcDotNetCore.Data
{
    public class UserProfile
    {
        public Int64 Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set;}
        public string Address { get; set; }
        public Int64 IdUser { get; set; }

        public virtual User User { get; set; }

        public ICollection<Article> Article { get; set; }
    }
}
