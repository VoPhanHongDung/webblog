﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebBlogMvcDotNetCore.Data
{
    public class UserProfileMap
    {
        public UserProfileMap(EntityTypeBuilder<UserProfile> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.FirstName).IsRequired();
            entityTypeBuilder.Property(t => t.LastName).IsRequired();
            entityTypeBuilder.Property(t => t.Email).IsRequired();
            entityTypeBuilder.Property(t => t.Address);
            entityTypeBuilder.HasOne(t => t.User).WithOne(u => u.UserProfile).HasForeignKey<UserProfile>(x => x.IdUser);
            entityTypeBuilder.HasMany(t => t.Article).WithOne(u => u.UserProfile).HasForeignKey(x => x.IdUserProfile);

        }
    }
}
