﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using WebBlogMvcDotNetCore.Data;

namespace WebBlogMvcDotNetCore.Repo
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) :base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new ArticleMap(modelBuilder.Entity<Article>());
            new CategoryMap(modelBuilder.Entity<Category>());
            new CommentMap(modelBuilder.Entity<Comment>());
            new UserMap(modelBuilder.Entity<User>());
            new UserProfileMap(modelBuilder.Entity<UserProfile>());
        }
    }
}
