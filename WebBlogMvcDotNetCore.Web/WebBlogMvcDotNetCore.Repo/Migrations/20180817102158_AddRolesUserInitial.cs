﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBlogMvcDotNetCore.Repo.Migrations
{
    public partial class AddRolesUserInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Roles",
                table: "User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Roles",
                table: "User");
        }
    }
}
