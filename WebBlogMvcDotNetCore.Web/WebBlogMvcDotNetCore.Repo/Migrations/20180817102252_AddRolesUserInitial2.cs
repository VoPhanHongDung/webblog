﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBlogMvcDotNetCore.Repo.Migrations
{
    public partial class AddRolesUserInitial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Roles",
                table: "User",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Roles",
                table: "User",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
