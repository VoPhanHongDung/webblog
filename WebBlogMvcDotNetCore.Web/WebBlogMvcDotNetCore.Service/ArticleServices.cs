﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebBlogMvcDotNetCore.Data;
using WebBlogMvcDotNetCore.Repo;
using System.Linq;

namespace WebBlogMvcDotNetCore.Service
{
    public class ArticleServices : IArticleServices
    {
        private IRepository<Article> _articleRepository;
       

        public ArticleServices(IRepository<Article> articleRepository)
        {
            this._articleRepository = articleRepository;
        }

        public IEnumerable<Article> GetArticle()
        {
            return _articleRepository.GetAll();
        }

        public Article GetArticle(long id)
        {
            return _articleRepository.Get(id);
        }

        public List<Article> GetRecentArticle()
        {
            List<Article> articleRecent = _articleRepository.GetAll().OrderByDescending(a => a.DateCreate).Take(3).ToList();
            return articleRecent;
                                
        }

        public void InsertArticle(Article article)
        {
            _articleRepository.Insert(article);
        }
    }
}
