﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebBlogMvcDotNetCore.Data;
using WebBlogMvcDotNetCore.Repo;

namespace WebBlogMvcDotNetCore.Service
{
    public class CommentServices : ICommentServices
    {
        private readonly IRepository<Comment> _commentRepository;
        public CommentServices(IRepository<Comment> commentRepository)
        {
            this._commentRepository = commentRepository;
        }
        public IEnumerable<Comment> GetComment(long idArticle)
        { 
            return _commentRepository.GetAll().Where(m => m.IdArticle == idArticle).OrderByDescending(d => d.DateCreate).Take(3).ToList();
        }

        public List<Comment> GetRecentComment()
        {
            return _commentRepository.GetAll().OrderByDescending(d => d.DateCreate).Take(2).ToList();
        }

        public void InsertComment(Comment comment)
        {
            _commentRepository.Insert(comment);
        }

       
    }
}
