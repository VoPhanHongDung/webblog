﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBlogMvcDotNetCore.Data;

namespace WebBlogMvcDotNetCore.Service
{
    public interface IArticleServices
    {
        IEnumerable<Article> GetArticle();
        Article GetArticle(long id);
        void InsertArticle(Article article);
        List<Article> GetRecentArticle();
        
    }
}
