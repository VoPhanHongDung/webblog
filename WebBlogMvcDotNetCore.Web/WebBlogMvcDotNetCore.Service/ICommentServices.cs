﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBlogMvcDotNetCore.Data;


namespace WebBlogMvcDotNetCore.Service
{
    public interface ICommentServices 
    {
        IEnumerable<Comment> GetComment(long idArticle);
        void InsertComment(Comment comment);
        List<Comment> GetRecentComment();
    }
}
