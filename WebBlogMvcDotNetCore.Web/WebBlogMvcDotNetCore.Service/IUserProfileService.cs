﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBlogMvcDotNetCore.Data;

namespace WebBlogMvcDotNetCore.Service
{
    public interface IUserProfileService
    {
        IEnumerable<UserProfile> GetUser();
        UserProfile GetUserProfile(long id);
        void InsertUserProfile(UserProfile user);
        void UpdateUserProfile(UserProfile user);
        UserProfile GetUserProfileByIdUser(long id);

    }
}
