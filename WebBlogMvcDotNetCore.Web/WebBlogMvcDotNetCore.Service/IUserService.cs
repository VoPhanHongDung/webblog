﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBlogMvcDotNetCore.Data;


namespace WebBlogMvcDotNetCore.Service
{
    public interface IUserService
    {
        IEnumerable<User> GetUser();
        User GetUserId(long id);
        void InsertUser(User user);
        void UpdateUser(User user);
        User SignIn(string userName, string password);
    }
}
