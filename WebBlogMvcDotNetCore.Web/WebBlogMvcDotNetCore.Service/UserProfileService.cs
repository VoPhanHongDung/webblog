﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBlogMvcDotNetCore.Data;
using WebBlogMvcDotNetCore.Repo;
using System.Linq;

namespace WebBlogMvcDotNetCore.Service
{
    public class UserProfileService : IUserProfileService
    {
        private IRepository<UserProfile> _userProfileRepository;

        public UserProfileService(IRepository<UserProfile> userProfileService)
        {
            this._userProfileRepository = userProfileService;
        }

        public IEnumerable<UserProfile> GetUser()
        {
            return _userProfileRepository.GetAll();
        }

        public UserProfile GetUserProfile(long id)
        {
            return _userProfileRepository.Get(id);
        }

        public UserProfile GetUserProfileByIdUser(long id)
        {
            var userProfileTemp = _userProfileRepository.GetAll().FirstOrDefault(u => u.IdUser == id);
            return userProfileTemp;
        }

        public void InsertUserProfile(UserProfile user)
        {
            _userProfileRepository.Insert(user);
        }

        public void UpdateUserProfile(UserProfile user)
        {
            _userProfileRepository.Update(user);
        }
    }
}
