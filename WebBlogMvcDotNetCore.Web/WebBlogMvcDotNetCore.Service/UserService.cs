﻿using System;
using System.Collections.Generic;
using System.Text;
using WebBlogMvcDotNetCore.Data;
using WebBlogMvcDotNetCore.Repo;
using System.Linq;


namespace WebBlogMvcDotNetCore.Service
{
    public class UserService :IUserService
    {
        private IRepository<User> _userRepository;
        private IRepository<UserProfile> _userProfileRepository;

        public UserService(IRepository<User> userRepository, IRepository<UserProfile> userProfileRepository)
        {
            this._userRepository = userRepository;
            this._userProfileRepository = userProfileRepository;
        }

        public IEnumerable<User> GetUser()
        {
            return _userRepository.GetAll();
        }

        public User GetUserId(long id)
        {
            return _userRepository.Get(id);
        }

        public void InsertUser(User user)
        {
            _userRepository.Insert(user);
        }

        public User SignIn(string userName, string password)
        {
            User userTemp = _userRepository.GetAll().FirstOrDefault(u => u.UserName == userName && u.Password == password);
            return userTemp;
        }

        public void UpdateUser(User user)
        {
            _userRepository.Update(user);
        }
    }
}
