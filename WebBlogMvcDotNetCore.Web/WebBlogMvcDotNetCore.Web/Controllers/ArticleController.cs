﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebBlogMvcDotNetCore.Web.Models;
using WebBlogMvcDotNetCore.Service;
using WebBlogMvcDotNetCore.Data;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.FileProviders;
using System.IO;
using System.Collections;
using Microsoft.AspNetCore.Hosting;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;

namespace WebBlogMvcDotNetCore.Web.Controllers
{
    [Authorize]
    public class ArticleController : Controller
    {
        private readonly IArticleServices _articleServices;
        private readonly ICommentServices _commentServices;
        const string SessionIdUserProfile = "_IdUserProfile";
        const string SessionUserName = "_UserName";

        private readonly IHostingEnvironment _hostingEnvironment;



        public ArticleController(IArticleServices articleServices, IHostingEnvironment hostingEnvironment, ICommentServices commentServices)
        {
            this._articleServices = articleServices;
            this._hostingEnvironment = hostingEnvironment;
            this._commentServices = commentServices;
        }


       
        public IActionResult Post()
        {
            TempData["UserName"] = HttpContext.Session.GetString(SessionUserName);
            TempData["IdUserProfile"] = HttpContext.Session.GetInt32(SessionIdUserProfile);

            return View();
        }

        [HttpPost]
        public IActionResult Post(ArticleModel model)
        {
            if (model.IdUserProfile != 0)
            {
                if (ModelState.IsValid)
                {
                    var img = getImg(model.Content).Replace("/images/", "");
                    Article article = new Article
                    {
                        Title = model.Title,
                        Content = model.Content,
                        Picture = img,
                        IdCategory = 1,
                        IdUserProfile = model.IdUserProfile,
                        DateCreate = model.DateCreate
                    };
                    _articleServices.InsertArticle(article);
                } else
                {
                    ViewBag.Error = "valid error";
                    return View();
                }
            } else
            {
                return RedirectToAction("SignIn", "User");
            }
            return RedirectToAction("Index", "Home");
        }

        public string getImg(string input)
        {
            try
            {
                string imgResult = "";
                imgResult = Regex.Match(input, "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase).Groups[1].Value;
                return imgResult;
            }
            catch
            {
                return "";
            }
        }

        [HttpPost("UploadFiles")]
        [Produces("application/json")]
        public async Task<IActionResult> UploadFile(List<IFormFile> files)
        {
            // Get the file from the POST request
            var theFile = HttpContext.Request.Form.Files.GetFile("file");

            // Get the server path, wwwroot
            string webRootPath = _hostingEnvironment.WebRootPath;

            // Building the path to the uploads directory
            var fileRoute = Path.Combine(webRootPath, "images");

            // Get the mime type
            var mimeType = HttpContext.Request.Form.Files.GetFile("file").ContentType;

            // Get File Extension
            string extension = System.IO.Path.GetExtension(theFile.FileName);

            // Generate Random name.
            string name = Guid.NewGuid().ToString().Substring(0, 8) + extension;

            // Build the full path inclunding the file name
            string link = Path.Combine(fileRoute, name);

            // Create directory if it does not exist.
            FileInfo dir = new FileInfo(fileRoute);
            dir.Directory.Create();

            // Basic validation on mime types and file extension
            string[] imageMimetypes = { "image/gif", "image/jpeg", "image/pjpeg", "image/x-png", "image/png", "image/svg+xml" };
            string[] imageExt = { ".gif", ".jpeg", ".jpg", ".png", ".svg", ".blob" };

            try
            {
                if (Array.IndexOf(imageMimetypes, mimeType) >= 0 && (Array.IndexOf(imageExt, extension) >= 0))
                {
                    // Copy contents to memory stream.
                    Stream stream;
                    stream = new MemoryStream();
                    theFile.CopyTo(stream);
                    stream.Position = 0;
                    String serverPath = link;

                    // Save the file
                    using (FileStream writerFileStream = System.IO.File.Create(serverPath))
                    {
                        await stream.CopyToAsync(writerFileStream);
                        writerFileStream.Dispose();
                    }

                    // Return the file path as json
                    Hashtable imageUrl = new Hashtable();
                    imageUrl.Add("link", "/images/" + name);

                    return Json(imageUrl);
                }
                throw new ArgumentException("The image did not pass the validation");
            }

            catch (ArgumentException ex)
            {
                return Json(ex.Message);
            }
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/Article/Detail/{idArticle?}")]
        public IActionResult Detail(int idArticle)
        {
            TempData["UserName"] = HttpContext.Session.GetString(SessionUserName);
            TempData["IdUserProfile"] = HttpContext.Session.GetInt32(SessionIdUserProfile);
            if (idArticle != 0)
            {
                var model = new CurrencyArticleAndComment();
                model.comments = _commentServices.GetComment(idArticle);
                model.article = _articleServices.GetArticle(idArticle);
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
           
        }
        [HttpPost]
        [Route("/Article/Detail/{idArticle?}")]
      
        public IActionResult Detail(IFormCollection form )
        {
            
            if (HttpContext.Session.GetInt32(SessionIdUserProfile) != 0)
            {
                Int64 idArticle =Int64.Parse(Request.Form["IdArticle"].ToString());
                string detailComment =Request.Form["DetailComment"].ToString();
                string fullName = Request.Form["FullName"].ToString();
                DateTime dateCreate =  DateTime.Parse(Request.Form["DateCreate"].ToString());
                Comment comment = new Comment
                {

                    DetailComment = detailComment,
                    Fullname = fullName,
                    DateCreate = dateCreate,
                    IdArticle = idArticle

                };

                _commentServices.InsertComment(comment);
                return RedirectToAction("Detail", "Article", new { @idArticle = idArticle });
            }
            else
            {
                return RedirectToAction("SignIn", "User");
            }
            
            
        }
    }

}