﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebBlogMvcDotNetCore.Service;
using WebBlogMvcDotNetCore.Data;
using WebBlogMvcDotNetCore.Web.Models;
using Microsoft.AspNetCore.Authorization;

namespace WebBlogMvcDotNetCore.Web.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IArticleServices _articleServices;
        public HomeController(IArticleServices articleServices)
        {
            this._articleServices = articleServices;
        }

        const string SessionUserName = "_UserName";
        const string SessionIdUserProfile = "_IdUserProfile";
        
        [AllowAnonymous]
        public IActionResult Index()
        {
            TempData["UserName"] = HttpContext.Session.GetString(SessionUserName);
            TempData["IdUserProfile"] = HttpContext.Session.GetInt32(SessionIdUserProfile);
            IEnumerable<Article> listArticle =  _articleServices.GetArticle();

            return View(listArticle);
        }
    }
}