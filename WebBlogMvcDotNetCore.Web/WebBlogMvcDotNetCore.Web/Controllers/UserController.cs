﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBlogMvcDotNetCore.Web.Models;
using Microsoft.AspNetCore.Mvc;
using WebBlogMvcDotNetCore.Service;
using WebBlogMvcDotNetCore.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace WebBlogMvcDotNetCore.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IUserProfileService _userProfileService;
        const string SessionUserName = "_UserName";
        const string SessionIdUserProfile = "_IdUserProfile";

        public UserController(IUserService userService,IUserProfileService userProfileService )
        {
            this._userService = userService;
            this._userProfileService = userProfileService;
           
        }
        
        [HttpGet]
        [AllowAnonymous]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult SignIn(UserModel model)
        {
            var result = _userService.SignIn(model.UserName, model.Password);
            var resultUserProfile = _userProfileService.GetUserProfileByIdUser(result.Id);
            if (result != null && resultUserProfile !=null)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, result.UserName));
                //claims.Add(new Claim(ClaimTypes.Role, result.Roles));
                

                var identity = new ClaimsIdentity( claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var principal = new ClaimsPrincipal(identity);

                var props = new AuthenticationProperties();
                props.IsPersistent = model.RememberMe;

                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props).Wait();


                HttpContext.Session.SetString(SessionUserName,result.UserName);
                HttpContext.Session.SetInt32(SessionIdUserProfile,(int) resultUserProfile.Id);
                TempData["UserName"] = HttpContext.Session.GetString(SessionUserName);
                TempData["IdUserProfile"] = HttpContext.Session.GetInt32(SessionIdUserProfile);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Error = "Error Sign In ! Please enter again";
                return View();
            }
        }

        [AllowAnonymous]
        public IActionResult SignOut()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            HttpContext.Session.SetString(SessionUserName,"");
            HttpContext.Session.Clear();
            return RedirectToAction("Index","Home");
        }

        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Register(UserModel model)
        {
            if (ModelState.IsValid)
            {
                User userEntity = new User
                {
                    UserName = model.UserName,
                    Password = model.Password
                };
                _userService.InsertUser(userEntity);

                if (userEntity.Id != 0)
                {
                    return RedirectToAction("UserProfile", new { @idUser = userEntity.Id });
                }
            }

            return View();
        }

        [HttpGet]
        [Route("/User/UserProfile/{idUser?}")]
        public IActionResult UserProfile(int IdUser)
        {
           
            UserProfileModel model = new UserProfileModel();
            if (IdUser != 0 )
            {
                model.IdUser = (long)IdUser;
            }else
            {
                return RedirectToAction("Register");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult UserProfile(UserProfileModel model)
        {
            if (ModelState.IsValid)
            {
                UserProfile userProfileEntity = new UserProfile
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Address = model.Address,
                    Email = model.Email,
                    IdUser = model.IdUser                   
                };
                _userProfileService.InsertUserProfile(userProfileEntity);
                if (userProfileEntity.Id > 0)
                {
                    return RedirectToAction("Detail",new { @idUserProfile = userProfileEntity.Id });
                }
            }
            return View(model);
        }
        [HttpGet]
        [Route("/User/Detail/{idUserProfile?}")]
        public IActionResult Detail(int IdUserProfile)
        {
            
            if(IdUserProfile != 0)
            {
                UserProfile userProfileEntity = _userProfileService.GetUserProfile((long)IdUserProfile);
                User userEntity = _userService.GetUserId(userProfileEntity.IdUser);
                var model = new CurrencyUserAndUserProfileModel { User = userEntity, UserProfile = userProfileEntity };
                return View(model);
            }else
            {
                return RedirectToAction("SignIn");
            }
            
        }

        

    }
}