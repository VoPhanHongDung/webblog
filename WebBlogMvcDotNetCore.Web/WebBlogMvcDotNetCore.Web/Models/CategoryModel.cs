﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace WebBlogMvcDotNetCore.Web.Models
{
    public class CategoryModel
    {
        [HiddenInput]
        public Int64 Id { get; set; }
        [StringLength(150)]
        public string DetailCategory { get; set; }
        [StringLength(20)]
        [Required]
        public string TitleCategory { get; set; }
    }

   
}
