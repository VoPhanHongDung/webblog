﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBlogMvcDotNetCore.Data;

namespace WebBlogMvcDotNetCore.Web.Models
{
    public class CurrencyUserAndUserProfileModel
    {
        public User User{ get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
