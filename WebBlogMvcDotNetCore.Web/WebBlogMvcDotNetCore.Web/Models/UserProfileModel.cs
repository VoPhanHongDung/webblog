﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace WebBlogMvcDotNetCore.Web.Models
{
    public class UserProfileModel
    {
        [HiddenInput]
        public Int64 Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Address { get; set; }

        [HiddenInput]
        public Int64 IdUser { get; set; }
    }
}
