﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBlogMvcDotNetCore.Service;
using WebBlogMvcDotNetCore.Data;
using Microsoft.AspNetCore.Mvc;
using WebBlogMvcDotNetCore.Web.Models;

namespace WebBlogMvcDotNetCore.Web.ViewComponents
{
    public class ArticleViewComponent : ViewComponent
    {
        private readonly IArticleServices _articleServices;
        public ArticleViewComponent(IArticleServices articleServices)
        {
            this._articleServices = articleServices;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
             var articleRecent = _articleServices.GetRecentArticle();
            TempData["articleRecent"] = articleRecent.Count();
            return View(articleRecent);
        }
         
    }
}
