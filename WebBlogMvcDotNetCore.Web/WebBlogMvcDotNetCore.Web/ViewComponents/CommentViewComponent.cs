﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBlogMvcDotNetCore.Service;

namespace WebBlogMvcDotNetCore.Web.ViewComponents
{
    public class CommentViewComponent :ViewComponent
    {
        private readonly ICommentServices _commentServices;
        public CommentViewComponent(ICommentServices commentServices)
        {
            this._commentServices = commentServices;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var commentRecent = _commentServices.GetRecentComment();
            return View(commentRecent);
        }
    }
}
